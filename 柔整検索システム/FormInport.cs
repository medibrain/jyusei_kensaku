﻿// ファイル名（電算管理番号）の数字チェックの有無
#define CHK_NUM

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace jyusei_kensaku
{
    public partial class FormInport : Form
    {
        /// <summary>取込元フォルダ （C:\Import）</summary>
        const string ImportFromDir = @"C:\Import";

        /// <summary>取込元フォルダに表示するコメント（空のファイル）</summary>
        const string ImportFromDirComment = @"C:\Import\★ここに取込元の画像を入れて下さい★";

        /// <summary>取込拡張子</summary>
        string[] ImportExt = {"gif", "jpeg", "jpg", "tif", "tiff", "png"};
        
        public FormInport()
        {
            InitializeComponent();
        }

        private void FormInport_Load(object sender, EventArgs e)
        {
            // 取り込み元のフォルダの表示
            label1.Text = "取込元\r\n" + ImportFromDir;
            if (Directory.Exists(ImportFromDir) == false) Directory.CreateDirectory(ImportFromDir);
            if (File.Exists(ImportFromDirComment) == false) File.WriteAllText(ImportFromDirComment, "");
            System.Diagnostics.Process.Start(ImportFromDir);
        }

        bool m_cancelFlag;

        /// <summary>バックグラウンド処理</summary>
        /// <param name="sender"></param>
        /// <param name="e">パラメータ：e.Argument=審査月(yyMM)</param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            long logNo = -1;

            try
            {
                e.Cancel = false;
                e.Result = null;


                // ファイルをサブディレクトリから移動、サブディレクトリは削除
                foreach (var subdir in Directory.GetDirectories(ImportFromDir))
                {
                    // ファイル移動 (サブディレクトリ→ルート（C:\Import）)
                    foreach (var subFile in Directory.GetFiles(subdir, "*.*", SearchOption.AllDirectories))
                    {
                        string dest = ImportFromDir + "\\" + Path.GetFileName(subFile);
                        if (File.Exists(dest)) File.Delete(subFile);
                        else File.Move(subFile, dest);
                    }
                    // サブディレクトリ削除
                    Directory.Delete(subdir, true);
                }

                // ディレクトリ名（KokuhoImage \ 取り込み日時 \ ）
                string root = "KokuhoImage\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss");
                int gid = 0;           // グループID
                int gcnt = 0;
                

                // ディレクトリ名（KokuhoImage \ 取り込み日時 \ 連番）
                string dir = root + "\\" + gid.ToString("0000");
                Directory.CreateDirectory(C_PostgreSettings.PictureRootPath + "\\" + dir);

                var files = Directory.GetFiles(ImportFromDir);
                for (int i = 0; i < files.Length; i++)
                {
                    if (m_cancelFlag)
                    {
                        e.Cancel = true;
                        break;
                    }

                    string fromPath = files[i];
                    string fn = Path.GetFileName(fromPath);
                    string id = Path.GetFileNameWithoutExtension(fromPath);
                    string ext = Path.GetExtension(fromPath).ToLower().Trim('.');

                    backgroundWorker1.ReportProgress(i * 100 / files.Length, fn);

                    if (fromPath == ImportFromDirComment) continue;

                    // 拡張子チェック
                    if (ImportExt.Contains(ext) == false)
                    {
                        File.Delete(fromPath);
                        continue;
                    }

#if CHK_NUM
                    // 電算番号チェック
                    long nId;
                    if (long.TryParse(id, out nId) == false || nId <= 0)
                    {
                        File.Delete(fromPath);
                        continue;
                    }
#endif

                    string destPath;
                    using (var r = C_GlobalData.sqlRead("SELECT foldername FROM t_fileseek WHERE filename = '{0}'", fn))
                    {
                        if (r.Read())
                        {
                            destPath = r["foldername"] + "\\" + fn;
                            if (destPath.IndexOf("KokuhoImage") == 0)
                            {
                                // 国保連画像ファイルの上書き（DBには既に登録されているので、登録しない）
                                try
                                {
                                    destPath = C_PostgreSettings.PictureRootPath + "\\" + destPath;
                                    if (File.Exists(destPath)) File.Delete(destPath);
                                    File.Move(fromPath, destPath);
                                }
                                catch (Exception)
                                {
                                }
                            }
                            else
                            {
                                // スキャン画像は上書きしない！
                                File.Delete(fromPath);
                            }
                            continue;
                        }
                    }

                    gcnt++;

                    if (gcnt > 1000)
                    {
                        gid++;
                        gcnt = 0;
                        dir = root + "\\" + gid.ToString("0000");       // ディレクトリ名（KokuhoImage \ 取り込み日時 \ gid）
                        Directory.CreateDirectory(C_PostgreSettings.PictureRootPath + "\\" + dir);
                    }

                    destPath = C_PostgreSettings.PictureRootPath + "\\" + dir + "\\" + fn;
                    
                    if (File.Exists(destPath)) File.Delete(destPath);
                    File.Move(fromPath, destPath);
                    C_GlobalData.sqlWrite("INSERT INTO t_pictures (id, filename, zokushi) VALUES ('{0}', '{1}', 1);"
                                        + "INSERT INTO t_fileseek (filename, foldername) VALUES ('{1}', '{2}');"
                                          , id, fn, dir);

                    if(logNo <= 0) logNo = C_GlobalData.WriteToReadlog(root, 0, 0);
                }

                C_GlobalData.WriteToReadlog(logNo, "取込完了");
            }
            catch (Exception ex)
            {
                e.Result = ex;
                C_GlobalData.WriteToReadlog(logNo, ex.Message);
            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            label1.Text = "取り込み中…\r\n" + e.UserState;
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {
                    label1.Text = "取込元\r\n" + ImportFromDir;
                    progressBar1.Value = 0;
                    button1.Text = "取込実行";
                }
                else if ((e.Result as Exception) != null)
                {
                    label1.Text = (e.Result as Exception).Message;
                    progressBar1.Value = 0;
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                label1.Text = ex.Message;
            }
        }
        
        /// <summary>取込実行 or キャンセル</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (backgroundWorker1.IsBusy)
                {
                    m_cancelFlag = true;
                    return;
                }
                else
                {
                    m_cancelFlag = false;
                    button1.Text = "キャンセル";
                    label1.Text = "取り込み中…";
                    backgroundWorker1.RunWorkerAsync();
                }

            }
            catch (Exception ex)
            {
                label1.Text = ex.Message;
            }
            
        }

        private void FormInport_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_cancelFlag = true;
            e.Cancel = (backgroundWorker1.IsBusy);
        }
    }
}
