﻿//#define BETA    // βバージョン （所要時間調査）
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Drawing.Printing;
using System.ComponentModel;
using System.Data;

namespace jyusei_kensaku
{   
    /// <summary>
    /// 柔整レセプト検索システムのメイン画面のクラス
    /// </summary>
    public partial class FormMain : Form
    {
        /// <summary> 表示列名、ヘッダテキスト、表示列名、ヘッダテキスト、…　のリスト </summary>
        string[] m_columnList = {
                                };


        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary> フォームOPEN時の処理 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
#if WAKA_KOIKI
            Text += "(和歌山広域連合)";
            buttonKokuImage.Visible = true;
#elif KADOMA_CITY            
            Text += "(門真市役所)";
            seek_Nisurecode.MaxLength = int.Parse(jyusei_kensaku.Properties.Resources.KADOMA_INSUREDNO_LEN);
            buttonKokuImage.Visible = false;
#else
#error　コンパイルエラー！ コンパイルスイッチにて、役所名を指定してください
#endif

#if BETA || DEBUG    // βバージョン （所要時間調査）
            DateTime beginTime = DateTime.Now;
            Text += "-βバージョン";
#endif
            // SQLOPEN
            C_GlobalData.sqlOpen();

#if BETA || DEBUG    // βバージョン （所要時間調査）
            MessageBox.Show("sqlOpen\r\n" + C_PostgreSettings.ConnectText + "\r\r" + (DateTime.Now - beginTime).TotalSeconds.ToString() + "秒");
#endif

            /**  */
            try
            {
                output_2_ListView();

                // ヘッダテキストの変更 (***は表示しない列)
                int DisplayIndex = 0;
                dataGrid.Columns["chk"].DisplayIndex = DisplayIndex++;              // 印刷チェック
                dataGrid.Columns["chk"].HeaderText = "";
                dataGrid.Columns["id"].DisplayIndex = DisplayIndex++;               // ID
                dataGrid.Columns["id"].HeaderText = "ID";
                dataGrid.Columns["id"].Visible = false;
#if KADOMA_CITY
                dataGrid.Columns["zokushi"].HeaderText = "***";
#else
                dataGrid.Columns["zokushi"].DisplayIndex = DisplayIndex++;          // 続紙枚数
                dataGrid.Columns["zokushi"].HeaderText = "枚数";
#endif
                dataGrid.Columns["sex"].HeaderText = "***";                         // sex (code)
                dataGrid.Columns["sex"].Visible = false;
                dataGrid.Columns["sexname"].DisplayIndex = DisplayIndex++;          // 性別
                dataGrid.Columns["sexname"].HeaderText = "性別";
                dataGrid.Columns["birth"].DisplayIndex = DisplayIndex++;            // 生年月日
                dataGrid.Columns["birth"].HeaderText = "生年月日";
                dataGrid.Columns["insuredno"].DisplayIndex = DisplayIndex++;        // 被保険者番号
                dataGrid.Columns["insuredno"].HeaderText = "被保険者番号";
                dataGrid.Columns["hoscode"].DisplayIndex = DisplayIndex++;          // 施術師コード
                dataGrid.Columns["hoscode"].HeaderText = "施術師コード";
                dataGrid.Columns["medimonth"].DisplayIndex = DisplayIndex++;        // 診療年月
                dataGrid.Columns["medimonth"].HeaderText = "診療年月";
                dataGrid.Columns["billmonth"].DisplayIndex = DisplayIndex++;        // 請求年月
                dataGrid.Columns["billmonth"].HeaderText = "請求年月";
                dataGrid.Columns["totalcost"].DisplayIndex = DisplayIndex++;        // 費用額
                dataGrid.Columns["totalcost"].HeaderText = "費用額";

                dataGrid.Columns["typecode"].HeaderText = "***";                    // typecode
#if KADOMA_CITY
                // 門真市は、区分、実日数、状態区分を表示しない
                dataGrid.Columns["typename"].HeaderText = "***";
                dataGrid.Columns["totalday"].HeaderText = "***";
                dataGrid.Columns["statecode"].HeaderText = "***";
#else
                dataGrid.Columns["typename"].DisplayIndex = DisplayIndex++;         // 区分
                dataGrid.Columns["typename"].HeaderText = "区分";
                dataGrid.Columns["totalday"].DisplayIndex = DisplayIndex++;         // 実日数
                dataGrid.Columns["totalday"].HeaderText = "実日数";
                dataGrid.Columns["statecode"].DisplayIndex = DisplayIndex++;        // 状態区分
                dataGrid.Columns["statecode"].HeaderText = "状態区分";
#endif
                dataGrid.Columns["filename"].HeaderText = "***";
                dataGrid.Columns["foldername"].HeaderText = "***";

                foreach (DataGridViewColumn col in dataGrid.Columns)
                {
                    if (col.HeaderText == "***") col.Visible = false;
                    col.ReadOnly = true;
                }

                dataGrid.Columns["chk"].ReadOnly = false;               // チェックボックス列 → 編集可能
                dataGrid.Columns["statecode"].ReadOnly = false;         // 状態区分列         → 編集可能


                //20190625104627 furukawa st ////////////////////////
                //西暦表示に変更
                
                dataGrid.Columns["birth"].DefaultCellStyle.Format = "yyyy/MM/dd";             // 生年月日
                dataGrid.Columns["medimonth"].DefaultCellStyle.Format = "yyyy/MM";           // 診療年月
                dataGrid.Columns["billmonth"].DefaultCellStyle.Format = "yyyy/MM";           // 請求年月

                //// セルの列を和暦対応にする
                //System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("ja-JP", false);
                //ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

                //dataGrid.Columns["birth"].DefaultCellStyle.Format = "gyy/MM/d";             // 生年月日
                //dataGrid.Columns["birth"].DefaultCellStyle.FormatProvider = ci;

                //dataGrid.Columns["medimonth"].DefaultCellStyle.Format = "gyy/MM";           // 診療年月
                //dataGrid.Columns["medimonth"].DefaultCellStyle.FormatProvider = ci;

                //dataGrid.Columns["billmonth"].DefaultCellStyle.Format = "gyy/MM";           // 請求年月
                //dataGrid.Columns["billmonth"].DefaultCellStyle.FormatProvider = ci;
                //20190625104627 furukawa ed ////////////////////////

                //20190625104721 furukawa st ////////////////////////
                //画面分割位置修正
                
                splitContainer1.SplitterDistance = (int)Math.Round(Screen.PrimaryScreen.WorkingArea.Width * 0.65);
                //20190625104721 furukawa ed ////////////////////////


                // Resize the DataGridView columns to fit the newly loaded content.
                dataGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);

                // バックグラウンドの関数の呼び出し (バックグラウンドで、描画を更新する！)
                backgroundWorker1.RunWorkerAsync();
            }
            catch (Exception) 
            {
            }
        }
            
        /// <summary>
        /// データビューへのDB出力
        /// </summary>
        /** データビューに出力する  */
        private void output_2_ListView()
        {            
            try
            {
                // クエリ + 検索条件 (where 部分) + 最大出力行数
                string sqlQuery = "SELECT * FROM v_receseek";

                var whereList = new List<string>();     // where条件のリスト （where andで区切る前のリスト）

                // 非保険者番号               (insuredno = '○○○')
                if (seek_Nisurecode.Text.Trim().Length > 0) whereList.Add(string.Format("insuredno = '{0}'", C_GlobalData.GetInsuredNo(seek_Nisurecode.Text)));
                
                // 医療機関番号               ( insuredno in ('●●●' , '△△△' … ) ) AND
                var hoscodeList = new List<string>(10);
                if (seek_Hoscode01.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode01.Text.Trim());
                if (seek_Hoscode02.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode02.Text.Trim());
                if (seek_Hoscode03.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode03.Text.Trim());
                if (seek_Hoscode04.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode04.Text.Trim());
                if (seek_Hoscode05.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode05.Text.Trim());
                if (seek_Hoscode06.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode06.Text.Trim());
                if (seek_Hoscode07.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode07.Text.Trim());
                if (seek_Hoscode08.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode08.Text.Trim());
                if (seek_Hoscode09.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode09.Text.Trim());
                if (seek_Hoscode10.Text.Trim().Length > 0) hoscodeList.Add(seek_Hoscode10.Text.Trim());
                if (hoscodeList.Count > 0) whereList.Add(string.Format("hoscode IN ('{0}')", string.Join("','", hoscodeList.ToArray())));

                string seekDate;
                // 診療年月
                seekDate = WarekiModule.warekiNum_2_sqlMaxMin(seek_MediDateMin.Text, seek_MediDateMax.Text, "medimonth");
                if (seekDate.Length > 0) whereList.Add (seekDate);
                
                // 請求年月
                seekDate = WarekiModule.warekiNum_2_sqlMaxMin(seek_BillDateMin.Text, seek_BillDateMax.Text, "billmonth");
                if (seekDate.Length > 0) whereList.Add(seekDate);

                // 費用額 (MIN MAX)
                uint min = C_GlobalData.string_2_uint32(seek_CostMin.Text);
                uint max = C_GlobalData.string_2_uint32(seek_CostMax.Text);
                if (min > max && max > 0)
                {
                    uint tmp = min;
                    min = max;
                    max = tmp;
                }
                if (min > 0) whereList.Add("( totalcost >= " + min.ToString() + ")");
                if (max > 0) whereList.Add("( totalcost <= " + max.ToString() + ")");

                if (whereList.Count > 0) sqlQuery += " WHERE " + string.Join(" AND ", whereList);

                sqlQuery += " LIMIT 1001";                                                                     // LIMIT句が使えるSQLの場合

                
                using(var dt = new DataTable())
                using(var da = C_GlobalData.sqlRead2DataAdapter(sqlQuery))
                {
                    dt.Columns.Add("chk", typeof(bool));                                                       // 印刷用のチェックボックスの追加
                    da.Fill(dt);

                    if (dt.Rows.Count > 1000) label_Count.Text = "検索結果\n1001件以上";
                    else label_Count.Text = string.Format("検索結果\n{0}件", dt.Rows.Count);


                    if (dt.Rows.Count > 0 && dt.Rows.Count <= 1000) label_Count.ForeColor = Color.Black;
                    else label_Count.ForeColor = Color.Red;

                    dataGrid.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
            }                
        }


        /// <summary>
        /// SQLをクローズする
        /// </summary>
        /** フォームCLOSE時の処理 */
        private void Form1_CLOSING(object sender, FormClosingEventArgs e)
        {
            backgroundWorker1.CancelAsync();
//            backgroundContinueFlag = false;     // バックグラウンドの停止 （フラグで管理）
            C_GlobalData.sqlClose();
        }


        /// <summary>
        /// 検索ボタン処理 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_seek_Click(object sender, EventArgs e)
        {
            //// 診療年月・請求年月・費用額は、入力値をチェックする。
            //if (WarekiModule.checkTextBoxForSeekWareki(seek_MediDateMin) == false) return;  // 診療年月

            output_2_ListView();
            
            dataGrid.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.DisplayedCells);
        }

        /// <summary>
        /// データグリッドの選択変更イベント 
        /// 表示するレセプト画像の変更を行う
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_Table_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                // 選択した行の電算管理番号を調べる
                label_MaxPage.Text = dataGrid.SelectedRows[0].Cells["zokushi"].Value.ToString();   // 最大ページ数(続紙枚数)の表示
                text_PageNo.Text = "1";                                                            // 1ページ目に戻る…
            }
            catch (Exception)
            {
            }
            showReceptPict();
        }

        /// <summary>
        /// ページ番号変更イベント
        /// 表示するレセプト画像の変更を行う
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void text_PageNo_TextChanged(object sender, EventArgs e)
        {
            showReceptPict();
        }

        /// <summary> DataGridViewの行 （フォルダ名・ファイル名）と ページ番号 から、画像ファイル名（フルパス）を取得する </summary>
        /// <param name="row">DataGridViewの行</param>
        /// <param name="pageNum">ページ番号</param>
        /// <returns></returns>
        private string pathName_from_id(DataGridViewRow row, uint pageNum)
        {
            try
            {
                string fileName = row.Cells["filename"].Value.ToString();
                if (pageNum > 1)
                {
                    fileName = Path.GetFileNameWithoutExtension(fileName) + "-" + pageNum.ToString() + Path.GetExtension(fileName);
                }

                return C_PostgreSettings.PictureRootPath + "\\" +
                       row.Cells["foldername"].Value.ToString() + "\\" +
                       fileName;
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// 表示するレセプト画像の変更を行う 
        /// </summary>
        private void showReceptPict()
        {
            try
            {
                // 選択した行の電算管理番号を調べる
                backgroundPicturePath = pathName_from_id(dataGrid.SelectedRows[0], uint.Parse(text_PageNo.Text));
            }
            catch (Exception)
            {
            }
        }


        /// <summary> バックグラウンドで表示させる画像ファイル（フルパス名）</summary>
        private string backgroundPicturePath = "";


        /// <summary> バックグラウンドの継続フラグ true:継続 / false:終了 </summary>
        private bool backgroundContinueFlag = true;

        /// <summary> 表示完了後の画像ファイル（フルパス名）</summary>
        private string previewdPicturePath = "";

        /// <summary>
        /// レセプト画像表示変更のバックグラウンド処理処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // バックグラウンド処理のループ （フォームを閉じるまで、延々と動かす）
            while (backgroundContinueFlag)
            {
                // 0.1秒停止
                try
                {
                    System.Threading.Thread.Sleep(500);
                }
                catch (Exception)
                {
                }

                // 表示の更新
                if (string.Compare(backgroundPicturePath, previewdPicturePath) != 0)
                {
                    showPicture();

                    //// 0.1秒停止
                    //try
                    //{
                    //    System.Threading.Thread.Sleep(500);
                    //}
                    //catch (Exception)
                    //{
                    //}
                }
            }

        }


        delegate void showPictureDelegate();

        void showPicture()
        {
            try
            {
                if (InvokeRequired)
                {
                    // 別スレッドから呼び出された場合
                    Invoke(new showPictureDelegate(showPicture));
                }
                else if(previewdPicturePath != backgroundPicturePath)
                {
                    //pictureBox.Image = System.Drawing.Image.FromFile(backgroundPicturePath, true);
                    pictureBox.ImageLocation = backgroundPicturePath;
                    previewdPicturePath = backgroundPicturePath;
                }
            }
            catch (Exception)
            {
            }
        }





        /// <summary>
        /// 前ページに移動するボタンの処理
        /// ページ番号を更新する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_prevPage_Click(object sender, EventArgs e)
        {   
            // ページNO　現在ページ数 - 1
            uint pageNo = C_GlobalData.string_2_uint32(text_PageNo.Text);
            if (pageNo > 1)
            {
                pageNo = pageNo - 1;
            }
            else
            {
                pageNo = 1;
            }

            try
            {            
                uint maxPage = uint.Parse(label_MaxPage.Text);
                if (pageNo > maxPage)
                {
                    pageNo = maxPage;       // ページNO の最大は、ファイルの件数
                }
            }
            catch (Exception)
            {
            }
            
            text_PageNo.Text = pageNo.ToString();
        }

        /// <summary>
        /// 次ページに移動するボタンの処理
        /// ページ番号を更新する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_nextPage_Click(object sender, EventArgs e)
        {
            // ページNO　現在ページ数 + 1
            uint pageNo = C_GlobalData.string_2_uint32(text_PageNo.Text) + 1;
            if (pageNo < 1)
            {
                pageNo = 1;             // ページNO の最小は 1
            }

            try
            {
                uint maxPage = uint.Parse(label_MaxPage.Text);
                if (pageNo > maxPage)
                {
                    pageNo = maxPage;       // ページNO の最大は、ファイルの件数
                }
            }
            catch (Exception)
            {
            }

            text_PageNo.Text = pageNo.ToString();
        }
                
        /// <summary>
        /// DataGridViewのセル変更イベント処理
        /// DBの状態区分コードを更新する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGrid.Columns[e.ColumnIndex].Name == "statecode")
                {
                    // 状態区分コードの列の場合
                    var id = dataGrid.Rows[e.RowIndex].Cells["id"].Value;        // 電算管理番号
                    var code = dataGrid[e.ColumnIndex, e.RowIndex].Value;        // 更新した行の状態区分コード

                    // SQLコマンドの実行
                    C_GlobalData.sqlWrite("UPDATE t_data SET statecode = {0} WHERE id = '{1}'" ,code, id);
                }

            }
            catch (Exception)
            { 
            }
        }

        /**
         * 印刷キュー
         * 印刷対象の画像ファイル（フルパス）一覧のキュー
         */
        List<Image> printList = new List<Image>();          // 印刷対象の画像のリスト
        uint printNowPage;                                  // 印刷対象の現在ページ

        /** 印刷イベント処理 */
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {   
            try {
                // 画像を取得
                Image img = printList[(int)printNowPage];

                // 単位を0.01インチ単位に合わせる
                float imageW = img.Width / 2;         // 画像幅 （ピクセル数 / 200dpi）インチ x100
                float imageH = img.Height / 2;        // 画像高 （ピクセル数 / 200dpi）インチ x100

                // 縦・横の調整 
                // 画像が横向き（幅>長）&& 画像の横がページ内（余白より内側）に収まらないとき、90度回転させる！
                if (imageW > imageH && imageW > e.MarginBounds.Width)
                {
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);    // 反転させずに時計回りに回転させる
                    var tmp = imageW;
                    imageW = imageH;
                    imageH = tmp;
                }

                // まず、サイズ合わせを行う！ （画像サイズか、ページサイズのうちの最小値）
                float w = Math.Min(imageW, e.MarginBounds.Width);         // 印刷幅 = 画像幅 or ページ幅 の最小
                float h = Math.Min(imageH, e.MarginBounds.Height);        // 印刷高 = 画像高 or ページ高 の最小

                // 縦横比の調整 (印刷範囲の縦横比を、元の画像と同じにする)
                float rate = Math.Min(w / imageW, h / imageH);          // 縮尺 = 印刷幅/画像幅 or 印刷高/画像高 の最小
                w = imageW * rate;
                h = imageH * rate;

                
                // 印刷位置のセンタリング (余白を調整する)
                // 余白 = 中心位置の差分 = (ページサイズ - 印刷サイズ ) / 2.0
                float x = (e.PageBounds.Width - w) / 2.0f;
                float y = (e.PageBounds.Height - h) / 2.0f;

                // プレビューではなく実際の印刷の時はプリンタの物理的な余白分ずらす
                if (((PrintDocument)sender).PrintController.IsPreview == false)
                {
                    x -= e.PageSettings.HardMarginX;
                    y -= e.PageSettings.HardMarginY;
                }

                // 印刷対象の画像のリストの現在ページを出力
                e.Graphics.DrawImage(img, x, y, w, h);  
            } catch(Exception)
            {
            }

            printNowPage ++;                            // 次のページにすすむ

            try {
                if (printNowPage < printList.Count)     // 現在ページ < ページ数 （次のページ有り）
                {   
                    e.HasMorePages = true;              // 次ページ＝true
                } 
                else
                {
                    e.HasMorePages = false;             // 次ページ＝false
                    printNowPage = 0;                   // 現在のページ＝0 に戻る
                }
            }
            catch (Exception)
            {
                e.HasMorePages = false;                 // 次ページ＝false
                printNowPage = 0;                       // 現在のページ＝0 に戻る
            }
        }



        /// <summary>
        /// 印刷ボタン（1件分）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void print1_Click(object sender, EventArgs e)
        {
            // 印刷を実行する（用紙・プリンタも設定する、プレビューなし）
            printDo(dataGrid.SelectedRows, false);
        }


        /// <summary>
        /// 印刷プレビューボタン（1件分）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printPreview1_Click(object sender, EventArgs e)
        {
            // 印刷を実行する（用紙・プリンタも設定する、プレビューあり）
            printDo(dataGrid.SelectedRows, true);
        }

        /// <summary>
        /// 印刷プレビューボタン（選択分）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printPreview2_Click(object sender, EventArgs e)
        {
            try
            {
                var list = new List<DataGridViewRow>();         // 印刷対象の行

                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    object val = row.Cells["chk"].Value;
                    if (val == null) { }
                    else if (val.GetType() != typeof(bool)) { }
                    else if ((bool)val == true)
                    {
                        list.Add(row);
                    }
                }

                // 印刷を実行する（用紙・プリンタも設定する、プレビューあり）
                printDo(list, true);

            }
            catch (Exception)
            {
            }
        }

        
        /// <summary> 用紙・プリンタを設定し、DataGridViewで選択した行の画像を印刷 </summary>
        /// <param name="rowList">DataGridViewの行一覧（印刷する画像の フォルダ名・ファイル名の一覧）</param>
        /// <param name="preview">true:プレビューあり。 false:プレビューなし</param>
        private void printDo(IEnumerable rowList, bool preview)
        {
            try
            {
                // 印刷リストの初期化
                printList.Clear();
                printNowPage = 0;


                foreach (DataGridViewRow row in rowList)
                {
                    long maxPage = Convert.ToInt64(row.Cells["zokushi"].Value); // 続紙枚数

                    for (uint i = 1; i <= maxPage; i++)
                    {
                        string path = pathName_from_id(row, i);
                        Image img = Image.FromFile(path);               // 画像の読み込み
                        printList.Add(img);                             // リストに追加                
                    }
                }


                if (printList.Count <= 0)
                {
                    MessageBox.Show("印刷対象なし", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    // printDocument.OriginAtMargins = true;

                    //　プリンタ名と余白設定を読み込む
                    string printerName; int printMargin;
                    C_GlobalData.getPrinterSetting(out printerName, out printMargin);


                    // 印刷ドキュメントに、プリンタを設定する。
                    if (printerName.Length > 0) printDocument.PrinterSettings.PrinterName = printerName;

                    // 用紙の向きを設定(横：true、縦：false)
                    printDocument.DefaultPageSettings.Landscape = false;

                    // 余白を0.01インチ単位に変換  (＝ 余白ミリ/ 25.4ミリ * 100) し、設定する
                    printMargin = (checkBox_Margin.Checked) ? (int)((double)printMargin * 100.0 / 25.4) : (0);
                    printDocument.DefaultPageSettings.Margins.Top = printMargin;
                    printDocument.DefaultPageSettings.Margins.Bottom = printMargin;
                    printDocument.DefaultPageSettings.Margins.Left = printMargin;
                    printDocument.DefaultPageSettings.Margins.Right = printMargin;


                    // 用紙設定（A4）
                    foreach (PaperSize ps in printDocument.PrinterSettings.PaperSizes)
                    {
                        if (ps.Kind == PaperKind.A4)
                        {
                            printDocument.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                            break;
                        }
                    }
                }


                if (preview)
                {
                    // 印刷プレビューの実行
                    printPreviewDialog.WindowState = FormWindowState.Maximized;
                    printPreviewDialog.ShowDialog();
                }
                else
                {
                    // 印刷の実行
                    printDocument.Print();
                }




                // 印刷対象のクリア
                foreach (Image img in printList)
                {
                    img.Dispose();
                }
                printList.Clear();

            }
            catch (Exception)
            {
            }

        }
        
        /// <summary>
        /// 全選択ボタン  チェックボックスをすべてONにする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxSelAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                using (var dt = dataGrid.DataSource as DataTable)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        row["chk"] = checkBoxSelAll.Checked;
                    }                    
                }
                dataGrid.FirstDisplayedScrollingColumnIndex = 0;
            }
            catch (Exception)
            {
            }

        }
        /// <summary>
        /// 読み込み ボタン 処理
        /// 読み込み ダイアログを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void settingButton_Click(object sender, EventArgs e)
        {
            new ReadDialog().ShowDialog();
        }


        /// <summary>
        /// 印刷設定ボタン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printSettingButton_Click(object sender, EventArgs e)
        {
            // プリンタ選択ダイアログを開く
            new SelectPrinterDialog().ShowDialog(this);
        }


        /// <summary>
        /// 表示列設定ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            // 表示列設定の実行
            columnSelect();
        }

        /// <summary>
        /// 表示列設定の実行
        /// </summary>
        private void columnSelect()
        {
            using (var dlg = new ColumnSelectDialog())
            {                
                foreach (DataGridViewColumn col in dataGrid.Columns)
                {
                    string head = col.HeaderText;
                    if (head != "" && head != "***")
                    {
                        dlg.checkedListBox1.Items.Add(head, col.Visible);
                    }
                }
                
                dlg.ShowDialog(dataGrid);

                // 表示設定ダイアログのCheckを、データグリッドビューの表示に反映させる。
                if (dlg.DialogResult == DialogResult.OK)
                {
                    foreach (DataGridViewColumn col in dataGrid.Columns)
                    {
                        string head = col.HeaderText;

                        if (dlg.checkedListBox1.Items.Contains(head))
                        {
                            bool check = dlg.checkedListBox1.CheckedItems.Contains(head);
                            col.Visible = check;
                        }
                    }
                }
            }
        }


        private void 表示列の設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 表示列設定の実行
            columnSelect();
        }

        private void 被保番をコピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGrid.CurrentCell == null) return;
                int cr = dataGrid.CurrentRow.Index;
                Clipboard.SetText(dataGrid["insuredno", cr].Value.ToString());
            }
            catch (Exception)
            {
            }
        }

        private void 施術師コードをコピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGrid.CurrentCell == null) return;
                int cr = dataGrid.CurrentRow.Index;
                Clipboard.SetText(dataGrid["hoscode", cr].Value.ToString());
            }
            catch (Exception)
            {
            }
        }

        private void 診療年月をコピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int cr = dataGrid.CurrentRow.Index;
                DateTime date = (DateTime)dataGrid["medimonth", cr].Value;
                Clipboard.SetText(C_GlobalData.date_2_wareki5(date));
            }
            catch (Exception)
            {
            }
        }

        private void 請求年月をコピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                int cr = dataGrid.CurrentRow.Index;
                DateTime date = (DateTime)dataGrid["billmonth", cr].Value;
                Clipboard.SetText(C_GlobalData.date_2_wareki5(date));
            }
            catch (Exception)
            {
            }
        }

        private void 費用額をコピーToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGrid.CurrentCell == null) return;
                int cr = dataGrid.CurrentRow.Index;
                Clipboard.SetText(dataGrid["totalcost", cr].Value.ToString());
            }
            catch (Exception)
            {
            }
        }

        


        /// <summary> 画像のサイズ（縮小表示）のモード一覧 </summary>
        enum PictureSizeMode{
            /// <summary> パネルに合わせる（スクロールしない） </summary>
            PanelSize,
            /// <summary> X・Yいずれかのみパネルに合わせる（一方はスクロールする） </summary>
            XorYSize,
            /// <summary> イメージの大きさに合わせる（X・Y両方向でスクロールする） </summary>
            ImageSize,
        } ;

        PictureSizeMode m_ePictureBoxSizeMode = PictureSizeMode.PanelSize;

        /// <summary>
        /// ピクチャーボックスクリック時、スクロールのモードを変える
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_Click(object sender, EventArgs e)
        {
            // 先に状態遷移させる。
            switch (m_ePictureBoxSizeMode)
            {
                case PictureSizeMode.PanelSize:
                    // パネルに合わせる（スクロールしない）→ X・Yいずれかのみパネルに合わせる（一方はスクロールする）
                    m_ePictureBoxSizeMode = PictureSizeMode.XorYSize;
                    break;
                case PictureSizeMode.XorYSize:
                    // X・Yいずれかのみパネルに合わせる（一方はスクロールする）→ イメージの大きさに合わせる（X・Y両方向でスクロールする）
                    m_ePictureBoxSizeMode = PictureSizeMode.ImageSize;
                    break;
                case PictureSizeMode.ImageSize:
                default:
                    // イメージの大きさに合わせる（X・Y両方向でスクロールする）→パネルに合わせる（スクロールしない） 
                    m_ePictureBoxSizeMode = PictureSizeMode.PanelSize;
                    break;
            }
            
            switch (m_ePictureBoxSizeMode)
            {
                case PictureSizeMode.PanelSize:
                    // パネルに合わせる（スクロールしない）
                    pictureBox.Dock = DockStyle.Fill;                   // ピクチャーボックスのサイズを、枠に合わせる
                    pictureBox.SizeMode = PictureBoxSizeMode.Zoom;      // 画像の大きさを、自動縮小（ピクチャーボックスに合わせる）
                    break;
                case PictureSizeMode.XorYSize:
                    // X・Yいずれかのみパネルに合わせる（一方はスクロールする）
                    pictureBox.SizeMode = PictureBoxSizeMode.Zoom;      // 画像の大きさを、自動縮小（ピクチャーボックスに合わせる）
                    break;
                case PictureSizeMode.ImageSize:
                    // イメージの大きさに合わせる（X・Y両方向でスクロールする）
                    pictureBox.Dock = DockStyle.Fill;
                    pictureBox.Dock = DockStyle.None;                   // ピクチャーボックスのサイズを、枠に合わせない
                    pictureBox.SizeMode = PictureBoxSizeMode.AutoSize;  // ピクチャーボックスの大きさを、画像に合わせる
                    break;
                default:
                    break;
            }
            updatePictureBoxSize();
        }

        /// <summary>
        /// ピクチャーボックスのイメージ読み込み完了時、全体表示（スクロールなし）に切り替える
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //pictureBox.Dock = DockStyle.Fill;
            //pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            updatePictureBoxSize();
        }

        /// <summary>
        /// ピクチャーボックス用のパネルサイズ変更時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel_PictureBox_SizeChanged(object sender, EventArgs e)
        {
            updatePictureBoxSize();
        }

        /// <summary>
        /// ピクチャーボックスのパネルサイズ変更時・イメージサイズ変更時に、高さ・幅を調整する。
        /// </summary>
        private void updatePictureBoxSize()
        {
            if (pictureBox.Image == null) return;

            // X・Yいずれかのみパネルに合わせる（一方はスクロールする）モードの時のみ、調整する
            if (m_ePictureBoxSizeMode == PictureSizeMode.XorYSize) 
            {
                try
                {
                    // 比率 (枠のサイズ / 画像のサイズ) を計算する
                    double rate_X = (double)panel_PictureBox.Width / (double)pictureBox.Image.Size.Width;
                    double rate_Y = (double)panel_PictureBox.Height / (double)pictureBox.Image.Size.Height;

                    if (rate_X > rate_Y) 
                    {
                        // X方向の比率 > Y方向の比率の時               
                        pictureBox.Dock = DockStyle.Top;                                // ピクチャーの幅を枠に合わせる
                        pictureBox.Height = (int)(pictureBox.Image.Height * rate_X);    // ピクチャーの高さを、X方向の比率に合わせる
                    }
                    else
                    {
//                        pictureBox.Height = panel_PictureBox.Height;
                        pictureBox.Dock = DockStyle.Left;                               // ピクチャーの幅を枠に合わせる
                        pictureBox.Width = (int)(pictureBox.Image.Width * rate_Y);      // ピクチャーの幅を、Y方向の比率に合わせる
                    }
                }
                catch (Exception)
                {
                }
            }

        }


        /// <summary>
        /// 診療年月（最大・最小）、請求年月（最大・最小）のテキストボックスの入力値を補正する （異常だった時に元に戻す）
        /// </summary>
        /// <param name="textBox">テキストボックス</param>
        /// <param name="prevText">もとのテキスト</param>
        /// <returns>新しいテキスト</returns>
        private void checkText_seekDate(object textBox, ref string prevText, ref int prevS, ref int prevLen)
        {
            try
            {
                string text = ((TextBox)textBox).Text.Trim();
                
                /// 数字かどうかのチェック
                uint num;
                if (uint.TryParse(text, out num) == false)
                {
                    ((TextBox)textBox).Text = prevText;
                    ((TextBox)textBox).Select(prevS, prevLen);
                    return;
                }


                if (text.Length >= 1)
                {
                }

                /// 新しいテキストを保持する
                prevText = text;
            }
            catch (Exception)
            {
                ;
            }
        }


        
        /// <summary> 各テキストボックスの前回入力値 </summary>
        private Dictionary<int, string> dic_PrevText = new Dictionary<int, string>();

        /// <summary> 各テキストボックスの前回選択位置 </summary>
        private Dictionary<int, int> dic_PrevSelStart = new Dictionary<int, int>();

        /// <summary> 各テキストボックスの前回選択の文字列長 </summary>
        private Dictionary<int, int> dic_PrevSelLen = new Dictionary<int, int>();

        /// <summary> 読込ログ </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonReadLog_Click(object sender, EventArgs e)
        {
            using (var f = new FormReadLog())
            {
                f.ShowDialog();
            }
        }

        
        /// <summary> CSV読込 </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCSV_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;
            using (var dlg = new CSVDialog(openFileDialog1.FileName))
            {
                dlg.ShowDialog();
            }             
        }





        private void dataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = false;
        }

        private void buttonKokuImage_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            using (var dlg = new FormInport()) dlg.ShowDialog();
            this.Visible = true;
        }


    }
}
