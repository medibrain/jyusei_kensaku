﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Net;
using System.Data.Common;

namespace jyusei_kensaku
{
    /// <summary>
    /// DVD読み込みダイアログ
    /// </summary>
    public partial class CSVDialog : Form
    {
        string m_CsvPath;

        /// <summary> コンストラクタ </summary>
        /// <param name="csvPath">CSV名（フルパス）</param>
        public CSVDialog(string csvPath = "")
        {
            InitializeComponent();
            m_CsvPath = csvPath;
            label_Fname.Text = Path.GetFileName(csvPath);
            label_Message_1.Text = "";

        }



        /// <summary>
        /// ダイアログ終了時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // バックグラウンド処理が実行中のとき、中断確認をする
                if (backgroundWorker1.IsBusy)
                {
                    // 中断確認のダイアログを表示
                    if (MessageBox.Show("読み込み途中です。中断しますか？",
                                         "読み込み中断",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        // YESボタンで、バックグラウンド処理ををキャンセルさせる
                        backgroundWorker1.CancelAsync();
                    }

                    // バックグラウンド処理が実行中 （終了しなかった）の場合、ダイアログは閉じない
                    //（スレッドキャンセルのイベントで閉じる）
                    if (backgroundWorker1.IsBusy) e.Cancel = true;
                }
            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// 読み込みのバックグラウンド処理処理 に 渡す引数を格納するクラス
        /// </summary>
        class backgroundArg
        {
            /// <summary> CSVパス </summary>
            public string csvPath = "";

            /// <summary> 読み込み対象の審査月（MIN）YYYYMM </summary>
            public int minMonth  = 0;
        }

        /// <summary>
        /// OKボタン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            // 2重実行防止 (スレッドが起動中で無い場合のみ動作させる！)
            if (backgroundWorker1.IsBusy) return;

            // 請求年月　平成y年m月
            uint y = 0, m = 0;
            uint.TryParse(textBoxY.Text, out y);
            uint.TryParse(textBoxM.Text, out m);

            // バックグラウンド処理に渡す値 （datainfo.txtの情報など）
            backgroundArg arg = new backgroundArg();
            
            arg.csvPath = m_CsvPath;
            arg.minMonth = (int)((1988 + y) * 100 + m);

                        
            // 読み込みのバックグラウンド処理の開始
            buttonOK.Enabled = false;                       // OKボタンの無効化
            label_Message_1.Text = "読み込み中...";
            backgroundWorker1.RunWorkerAsync(arg);
        }


        bool CheckExit(long logno)
        {
            if (backgroundWorker1.CancellationPending == true)
            {
                // 読込ログの出力
                C_GlobalData.WriteToReadlog(logno, "キャンセル");
                return true;
            }
            else return false;
        }

#if DEBUG
        DateTime m_BeginTime;
#endif

        /// <summary>
        /// 読み込みのバックグラウンド処理処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> backgroundArg でキャストする</param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
#if DEBUG
            m_BeginTime = DateTime.Now;
#endif
            backgroundArg arg = (backgroundArg)e.Argument;      // 入力 backgroundArg でキャストする

            e.Cancel = false;                                   // キャンセル = false
            
            // 読込ログの出力 ()
            long logno =C_GlobalData.WriteToReadlog(Path.GetFileName(arg.csvPath), 0, 0);
            
            // 既存のテーブルから、個人番号一覧を読込
            var hs_id = new HashSet<string>();                                                            // 個人番号一覧 (INSERT UPDATEの切り替え用)
            var hs_all = new HashSet<string>();                                                           // 既存データ一覧 (重複時はUPDATEしない)
            using (var cmd = C_GlobalData.sqlCommand("SELECT id, sex, birth, insuredno, hoscode, medimonth, billmonth, typecode, totalcost, totalday, statecode FROM t_data"))
            using (var dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    hs_id.Add(dr[0].ToString());
                    string val = "";
                    for (int i = 0; i < dr.FieldCount; i++) val += dr[i].ToString() + "\t";
                    hs_all.Add(val);

                    if (CheckExit(logno)) return;
                }
            }


            string[] csvLines = File.ReadAllLines(arg.csvPath, System.Text.Encoding.Default);          // CSVファイルの一括読込 (SJIS)
                        
            // コマンド発行 cmds[0]:INSERT  cmsd[1]:UPDATE
            var cmds = new DbCommand[2];
#if SQLIGHT
            using (var tran = C_GlobalData.sqlBeginTransaction())
#endif
            using (cmds[0] = C_GlobalData.sqlCommand("INSERT INTO t_data (id, sex, birth, insuredno, hoscode, medimonth, billmonth, typecode, totalcost, totalday, statecode) VALUES (:id, :sex, :birth, :insuredno, :hoscode, :medimonth, :billmonth, :typecode, :totalcost, :totalday, :statecode)"))
            using (cmds[1] = C_GlobalData.sqlCommand("UPDATE t_data SET sex = :sex, birth = :birth, insuredno = :insuredno, hoscode = :hoscode, medimonth = :medimonth, billmonth = :billmonth, typecode = :typecode, totalcost = :totalcost , totalday = :totalday, statecode = :statecode WHERE id = :id"))
            {
                for (int i = 0; i < 2; i++)
                {
                    C_GlobalData.AddDBParam(cmds[i], "id", C_GlobalData.e_dbType.Text);                  //  0
                    C_GlobalData.AddDBParam(cmds[i], "sex", C_GlobalData.e_dbType.Integer);              //  1
                    C_GlobalData.AddDBParam(cmds[i], "birth", C_GlobalData.e_dbType.Date);               //  2
                    C_GlobalData.AddDBParam(cmds[i], "insuredno", C_GlobalData.e_dbType.Text);           //  3
                    C_GlobalData.AddDBParam(cmds[i], "hoscode", C_GlobalData.e_dbType.Bigint);           //  4
                    C_GlobalData.AddDBParam(cmds[i], "medimonth", C_GlobalData.e_dbType.Date);           //  5
                    C_GlobalData.AddDBParam(cmds[i], "billmonth", C_GlobalData.e_dbType.Date);           //  6
                    C_GlobalData.AddDBParam(cmds[i], "typecode", C_GlobalData.e_dbType.Integer);         //  7
                    C_GlobalData.AddDBParam(cmds[i], "totalcost", C_GlobalData.e_dbType.Integer);        //  8
                    C_GlobalData.AddDBParam(cmds[i], "totalday", C_GlobalData.e_dbType.Integer);         //  9
                    C_GlobalData.AddDBParam(cmds[i], "statecode", C_GlobalData.e_dbType.Integer);        // 10                        
                }


                var hs_csv = new HashSet<string>();                                               // CSVに含まれている個人番号一覧 (同じIDは読まない・最後尾優先)


                for (int i = 0; i < csvLines.Length; i++)
                {
                    var line = csvLines[csvLines.Length - i - 1];                                 // 最後尾から読む

                    try
                    {
                        if (CheckExit(logno)) return;                                             // キャンセル確認
                        
                        // プログレスバー表示
                        backgroundWorker1.ReportProgress(100 * i / csvLines.Length, string.Format("{0} / {1}", i + 1, csvLines.Length));                        
        
                        string[] strs = line.Replace("\"", "").Split(',');

                        
#if WAKA_KOIKI          
                        int tmpInt;
                        // 和歌山広域連合向けのCSV取込
                        if (strs.Length < 68) continue;
                        if (int.TryParse(strs[0], out tmpInt) == false) continue;         // 請求年月のチェック
                        if (tmpInt < arg.minMonth) continue;

                        string id = strs[66].Trim();                                      // 電算管理番号
                        if (id == "") continue;
                        if (hs_csv.Contains(id)) continue;                                // CSV上で電算管理番号が重複 → 読まない
                        hs_csv.Add(id);

                        // 発行するコマンドの切り替え  重複有り→cmds[1] (INSERT)  :  なし→cmds[0] (UPDATE)
                        var cmd = (hs_id.Contains(id) ? cmds[1] : cmds[0]);
                        cmd.Parameters["id"].Value = id;                                              // 電算管理番号
                        cmd.Parameters["sex"].Value = csv_text_2_int(strs[11]);                      // 性別
                        cmd.Parameters["birth"].Value = csv_wareki_2_dateTime(strs[12]);             // 生年月日（和暦コード → DateTime）
                        cmd.Parameters["insuredno"].Value = strs[1].Trim();                           // 被保険者番号                        
                        long hoscode = 0;
                        if (int.TryParse(strs[5], out tmpInt)) hoscode += (long)tmpInt * 100000000L;  //        00 都道府県コード
                        if (int.TryParse(strs[2], out tmpInt)) hoscode += (long)tmpInt * 10000000L;   //        0  区分（7:鍼灸、8：あんま、9：柔整）                    
                        if (int.TryParse(strs[6], out tmpInt)) hoscode += (long)tmpInt * 100000L;     //      00   市町村コード
                        if (int.TryParse(strs[7], out tmpInt)) hoscode += tmpInt;                     // 00000     医療機関コード                            
                        cmd.Parameters["hoscode"].Value = hoscode;                                    // 施術師コード

                        cmd.Parameters["medimonth"].Value = csv_yyyyMM_2_dateTime(strs[4]);           // 診療年月 （YYYYMM → DateTime）
                        cmd.Parameters["billmonth"].Value = csv_yyyyMM_2_dateTime(strs[0]);           // 請求年月 （YYYYMM → DateTime）
                        cmd.Parameters["typecode"].Value = csv_text_2_int(strs[2]);                   // 区分（7:鍼灸、8：あんま、9：柔整）
                        cmd.Parameters["totalcost"].Value = csv_text_2_int(strs[45]);                 // 費用額
                        cmd.Parameters["totalday"].Value = csv_text_2_int(strs[18]);                  // 実日数
                        cmd.Parameters["statecode"].Value = csv_text_2_int(strs[65]);                 // 状態区分コード
#elif KADOMA_CITY       
                        // 門真市役所向けのCSV取込
                        if (strs.Length < 60) continue;

                        string id = strs[1].Trim();                                                   // 電算管理番号 (レセプト全国共通キー)
                        if (id == "") continue;

                        if (hs_csv.Contains(id)) continue;                                            // CSV上で電算管理番号が重複 → 読まない
                        hs_csv.Add(id);

                        // 発行するコマンドの切り替え  重複有り→cmds[1] (INSERT)  :  なし→cmds[0] (UPDATE)
                        DbCommand cmd = (hs_id.Contains(id) ? cmds[1] : cmds[0]);
                        cmd.Parameters["id"].Value = id;                                              // 電算管理番号(レセプト全国共通キー) 
                        cmd.Parameters["sex"].Value = csv_text_2_int(strs[6]);                        // 性別      
                        cmd.Parameters["birth"].Value = csv_wareki_2_dateTime(strs[5]);               // 生年月日（和暦コード → DateTime）
                        cmd.Parameters["insuredno"].Value = C_GlobalData.GetInsuredNo(strs[4]);       // 被保険者番号
                        long hoscode;                                                                 // 施術師コード （医科・歯科・調剤も取り込む）
                        if (long.TryParse(strs[2].Trim(), out hoscode) == false) continue;            //
                        else cmd.Parameters["hoscode"].Value = hoscode;                               //

                        cmd.Parameters["billmonth"].Value = csv_eyyMM_2_dateTime(strs[0]);            // 請求年月 （eYYMM → DateTime）
                        cmd.Parameters["medimonth"].Value = csv_eyyMM_2_dateTime(strs[13]);           // 診療年月 （eYYMM → DateTime）
                        //cmd.Parameters["typecode"].Value =null;                                     // 区分（7:鍼灸、8：あんま、9：柔整）
                        cmd.Parameters["totalcost"].Value = csv_text_2_int(strs[59]);                 // 費用額（国保決定点数）
                        //cmd.Parameters["totalday"].Value = null;                                    // 実日数
                        //cmd.Parameters["statecode"].Value =null;                                    // 状態区分コード
#else
#error　コンパイルエラー！ コンパイルスイッチにて、役所名を指定してください
#endif

                        // 重複チェック （登録不要）
                        string val = "";
                        foreach (DbParameter p in cmd.Parameters) val += ((p.Value != null) ? (p.Value.ToString()) : ("")) + "\t";
                        if(hs_all.Contains(val)) continue;
                        
                        cmd.ExecuteNonQuery();


                        if (hs_id.Contains(id) == false) hs_id.Add(id);

                    }
                    catch (Exception ex)
                    {
                    }
                }

                
#if SQLIGHT
                tran.Commit();
#endif
            }


            // 読込ログの出力
            C_GlobalData.WriteToReadlog(logno, "読込完了");
            
            // バックグラウンド処理完了 ⇒ キャンセルではない！
            e.Cancel = false;
        }

        /// <summary>
        /// OKボタンバックグラウンドの終了処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
#if DEBUG
            MessageBox.Show("読み込み完了\r\n所要時間：" +   (DateTime.Now - m_BeginTime).TotalSeconds + "秒");
#endif

            this.Close();           // ダイアログを閉じる
        }


        /// <summary>
        /// ProgressBarの更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            label_Message_1.Text = e.UserState.ToString();
            progressBar.Value = e.ProgressPercentage;
        }

        
                
        object csv_text_2_int(string text)
        {
            int n;
            if (int.TryParse(text, out n)) return n;
            else return null;
        }


        /// <summary>
        /// 和暦 ⇒ DateTime型 への変換
        /// </summary>
        /// <param name="wareki">和暦、"gYYMMDD"の７文字、 g は元号(0が明治～4が平成)、 YYMMDDは年月日 </param>
        /// <returns>DateTime型 </returns>
        object csv_wareki_2_dateTime(string wareki)
        {
            try
            {
                wareki = wareki.Trim();

                int e, y, m, d;                                  // 元号・年・月・日
                if (wareki.Length < 7) return null;
                if (int.TryParse(wareki.Substring(0, 1), out e) == false) return null;
                if (int.TryParse(wareki.Substring(1, 2), out y) == false) return null;
                if (int.TryParse(wareki.Substring(3, 2), out m) == false) return null;
                if (int.TryParse(wareki.Substring(5, 2), out d) == false) return null;
                if (e <= 0 || e > 4 || y <= 0 || m <= 0 || m > 12 || d <= 0 || d > 31) return null;

                // 西暦への変換
                y = WarekiModule.wareki_2_seireki(e, y);
                return new DateTime(y, m, d);
            }
            catch (Exception)
            {
                return null;
            }
        }



        //20190621150340 furukawa st ////////////////////////
        //令和追加        
        static int[] e_2_yyyy = { 0, 1867, 1911, 1925, 1988 ,2019};
            //static int[] e_2_yyyy = { 0, 1867, 1911, 1925, 1988 };
        //20190621150340 furukawa ed ////////////////////////



        /// <summary> eyyMM (和暦五桁) ⇒ DateTime型 への変換 </summary>
        /// <param name="eyyMM"></param>
        /// <returns>DateTime型 </returns>
        object csv_eyyMM_2_dateTime(string eyyMM)
        {
            try
            {
                int e = int.Parse(eyyMM.Substring(0, 1));
                int y = int.Parse(eyyMM.Substring(1, 2));
                int m = int.Parse(eyyMM.Substring(3, 2));

                //2020年4月15日古川門真令和不具合対策
                if (e == 5 && y == 1 && m < 5)
                {
                    return new DateTime(e_2_yyyy[e - 1] + y, m, 1);
                }
                else if (e == 5 && y == 1 && m >= 5)
                {
                    return new DateTime(e_2_yyyy[e] + y, m, 1);
                }
                else if (e == 5 && y >= 2)
                {
                    return new DateTime(e_2_yyyy[e] + y-1, m, 1);
                }
                else
                {
                    return new DateTime(e_2_yyyy[e] + y, m, 1);
                }
                //return new DateTime(e_2_yyyy[e] + y, m, 1);
            }
            catch (Exception)
            {

                return null;
            }
        }



        /// <summary>
        /// yyyyMM ⇒ DateTime型 への変換
        /// </summary>
        /// <param name="yyyyMM"></param>
        /// <returns>DateTime型 </returns>
        object csv_yyyyMM_2_dateTime(string yyyyMM)
        {
            try
            {
                int y, m;                                  // 年・月
                yyyyMM = yyyyMM.Trim();
                if (int.TryParse(yyyyMM.Substring(0, 4), out y) == false) return null;
                if (int.TryParse(yyyyMM.Substring(4, 2), out m) == false) return null;
                if (y <= 0 || m <= 0 || m > 12) return null;

                return new DateTime(y, m, 1);
            }
            catch (Exception)
            {
                return null;
            }
        }


        private void textBoxM_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                var textbox = (Control)sender;
                string s = "";
                foreach (char c in textbox.Text)
                {
                    if (c >= '0' && c <= '9') s += c;
                }
                textbox.Text = s;

                //20190621154215 furukawa st ////////////////////////
                //和暦表示機能
                
                lblWareki.Text=WarekiModule.seireki2wareki(int.Parse(textBoxY.Text), int.Parse(textBoxM.Text));
                //20190621154215 furukawa ed ////////////////////////

            }
            catch (Exception)
            {
            }
        }
    }
}

