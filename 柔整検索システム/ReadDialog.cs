﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;
using System.Threading;
using System.Net;

namespace jyusei_kensaku
{
    /// <summary>
    /// DVD読み込みダイアログ
    /// </summary>
    public partial class ReadDialog : Form
    {
        public ReadDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ダイアログ初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadDialog_Load(object sender, EventArgs e)
        {
            try
            {
                // ドライブ選択のコンボボックスの設定
                combo_DrivaName.Items.Clear();                              // クリア
                foreach (string drive in Directory.GetLogicalDrives())      // ローカルドライブをコンボボックスに追加
                {
                    combo_DrivaName.Items.Add(drive);
                }
                combo_DrivaName.Text = C_GlobalData.driveName;              // ドライブ名は、XMLから取得 （C_GlobalData のプロパティ）


                // 前回読み込んだ ドライブの情報を読み込む
                string prevId; uint prevDiscno; uint prevDisccount;
                C_GlobalData.getDataInfoFromXML(out prevId, out prevDiscno, out prevDisccount);

                if (prevDiscno == 0)        // 前回のディスクが０枚目
                {
                    label_Message_1.Text = "1枚目のディスクをセットしてください";
                    label_Message_2.Text = "";
                }
                else if (prevDiscno < prevDisccount)   // 前回のディスクが１枚目以降 （ただし最終ではない）
                {
                    label_Message_1.Text = (prevDiscno + 1).ToString() + "枚目のディスクをセットしてください";
                    label_Message_2.Text = "前回：" + prevDiscno.ToString() + "枚目 (dataid:" + prevId + ")";
                }
                else
                {
                    label_Message_1.Text = "1枚目のディスクをセットしてください";
                    label_Message_2.Text = "前回：" + prevDiscno.ToString() + "枚目 (dataid:" + prevId + ")";
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// ダイアログ終了時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReadDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                // コンボボックスで選択したドライブ名を、XMLに出力 （C_GlobalData のプロパティ）
                C_GlobalData.driveName = combo_DrivaName.Text;


                // バックグラウンド処理が実行中のとき、中断確認をする
                if (backgroundWorker1.IsBusy)
                {
                    // 中断確認のダイアログを表示
                    if (MessageBox.Show("読み込み途中です。中断しますか？",
                                         "読み込み中断",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        // YESボタンで、バックグラウンド処理ををキャンセルさせる
                        backgroundWorker1.CancelAsync();
                    }

                    // バックグラウンド処理が実行中 （終了しなかった）の場合、ダイアログは閉じない
                    //（スレッドキャンセルのイベントで閉じる）
                    if (backgroundWorker1.IsBusy) e.Cancel = true;
                }
            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// DVDドライブから datainfo.txt のパスを探す。 
        /// </summary>
        /// <param name="driveName">ドライブ名 (例："D:\\")</param>
        /// <returns>datainfo.txt があるディレクトリ (通常はD:) 見つからないときはNULLを返す</returns>
        private string findDataInfoDir(string driveName)
        {
            // ROOTから datainfo.txtを探す
            string[] files;
            try
            {
                files = Directory.GetFiles(driveName, "datainfo.txt");
                if (files.Length > 0) return Path.GetDirectoryName(files[0]);
            }
            catch (Exception)
            {
            }

            // ROOTから見つからない場合、サブディレクトリから探す (1階層だけ許す)
            try
            {                
                foreach (string subDir in Directory.GetDirectories(driveName))
                {
                    // subDirから datainfo.txtを探す
                    files = Directory.GetFiles(subDir, "datainfo.txt");
                    if (files.Length > 0) return Path.GetDirectoryName(files[0]);                    
                }
            }
            catch (Exception)
            {
            }

            return null;
        }

        /// <summary>
        /// datainfo.txt ファイルから、dataid: (ディスクのID)、 disccount(合計枚数)、 discno(現在のディスクNO)を読み込む
        /// </summary>
        /// <param name="path">datainfo.txt ファイルのパス</param>
        /// <param name="dataid">ディスクのID</param>
        /// <param name="discno">現在のディスクNO</param>
        /// <param name="disccount">合計枚数 </param>
        /// <returns></returns>
        private bool readDataInfo(string path, out string dataid, out uint discno, out uint disccount)
        {
            dataid = "";
            disccount = 0;
            discno = 0;

#if !DEBUG
            try
#endif
            {
                // datainfo.txt ファイルの全行読込み
                var allLines = File.ReadLines(path, Encoding.GetEncoding("Shift_JIS"));
                foreach (var line in allLines)
                {
                    if (line.ToLower().IndexOf("dataid:") >= 0) dataid = line.ToLower().Replace("dataid:", "");
                    if (line.ToLower().IndexOf("disccount:") >= 0) disccount = uint.Parse(line.ToLower().Replace("disccount:", ""));
                    if (line.ToLower().IndexOf("discno:") >= 0) discno = uint.Parse(line.ToLower().Replace("discno:", ""));
                }
            }
#if !DEBUG
            catch (Exception)
            {
                    return false;
            }
#endif
           if (dataid.Length > 0 && disccount > 0 && discno > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// OKボタン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            // 2重実行防止 (スレッドが起動中で無い場合のみ動作させる！)
            if (backgroundWorker1.IsBusy) return;        
            
            // バックグラウンド処理に渡す値 （datainfo.txtの情報など）
            backgroundArg arg = new backgroundArg();

            // ドライブ名をXMLに出力
            C_GlobalData.driveName = combo_DrivaName.Text;

            // ドライブから、 datainfo.txt を探す
            arg.rootDir　= findDataInfoDir(combo_DrivaName.Text);
            if (arg.rootDir == null)
            {
                MessageBox.Show("正しいディスクをセットしてください！！\r\ndatainfo.txt なし",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

                return;
            }

            // datainfo.txt から ディスクのID・ディスクNO・ディスクの総枚数を読む            
            if (readDataInfo(arg.rootDir + @"\datainfo.txt", out arg.dataInfoDataid, out arg.dataInfoDiscno, out arg.dataInfoDisccount) == false)
            {
                MessageBox.Show("読み込み失敗",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);


                return;
            }


            // 読込ログの出力 ()
#if POSTGRESQL
            using (var cmd = C_GlobalData.sqlCommand("INSERT INTO t_ReadLog (col_readtime, col_dataid, col_discno, col_disccount, col_pcname, col_info) VALUES (:col_readtime, :col_dataid, :col_discno, :col_disccount, :col_pcname, :col_info) RETURNING col_logno"))
#elif SQLIGHT
            using (var cmd = C_GlobalData.sqlCommand("INSERT INTO t_ReadLog (col_readtime, col_dataid, col_discno, col_disccount, col_pcname, col_info) VALUES (:col_readtime, :col_dataid, :col_discno, :col_disccount, :col_pcname, :col_info);" +
                                                     "SELECT MAX(col_logno) FROM t_ReadLog"))
#endif
            {
                C_GlobalData.AddDBParam(cmd, "col_readtime", C_GlobalData.e_dbType.Timestamp, DateTime.Now);
                C_GlobalData.AddDBParam(cmd, "col_dataid", C_GlobalData.e_dbType.Text, arg.dataInfoDataid);
                C_GlobalData.AddDBParam(cmd, "col_discno", C_GlobalData.e_dbType.Integer, arg.dataInfoDiscno);
                C_GlobalData.AddDBParam(cmd, "col_disccount", C_GlobalData.e_dbType.Integer, arg.dataInfoDisccount);
                C_GlobalData.AddDBParam(cmd, "col_pcname", C_GlobalData.e_dbType.Text, Dns.GetHostName());
                C_GlobalData.AddDBParam(cmd, "col_info", C_GlobalData.e_dbType.Text, "");

                using (var r = cmd.ExecuteReader())
                {
                    if (r.Read())
                    {
                        long seleal;
                        if (long.TryParse(r[0].ToString(), out seleal)) arg.logno = seleal;
                    }

                }

            }
            
            // 前回読み込んだ ドライブの情報を読み込む
            string prevID; uint prevDiscno; uint prevDisccount;
            C_GlobalData.getDataInfoFromXML(out prevID, out prevDiscno, out prevDisccount);

            if (prevDiscno >= prevDisccount)    // 次は1枚目を読み込む場合  （前回の discno >= 前回の disccount 、 前回で最後まで読み込んだ）
            {
                if (arg.dataInfoDiscno > 1)    // dvdが１枚目でない
                {
                    string msg = "1枚目ではありません。\r\n" +
                                 arg.dataInfoDiscno.ToString() + "枚目 (dataid:" + arg.dataInfoDataid + ") を読み込みますか？";
                    if (MessageBox.Show(msg, "読み込み確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        if (arg.logno != null)
                        {
                            C_GlobalData.sqlWrite("UPDATE t_ReadLog SET col_info = '{0}' WHERE col_logno = {1}", "キャンセル", arg.logno);
                        }
                        return;
                    }
                }
            }
            else
            {
                if (arg.dataInfoDataid != prevID || arg.dataInfoDiscno != prevDiscno + 1)
                {
                    string msg = "前回の続きではありません。\r\n" +
                                 arg.dataInfoDiscno.ToString() + "枚目 (dataid:" + arg.dataInfoDataid + ") を読み込みますか？";
                    if (MessageBox.Show(msg, "読み込み確認", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        if (arg.logno != null)
                        {
                            C_GlobalData.sqlWrite("UPDATE t_ReadLog SET col_info = '{0}' WHERE col_logno = {1}", "キャンセル", arg.logno);
                        }
                        return;
                    }
                }
            }

            // 読み込みのバックグラウンド処理の開始
            buttonOK.Enabled = false;                       // OKボタンの無効化
            label_Message_1.Text = "読み込み中...";
            label_Message_2.Text = arg.dataInfoDiscno.ToString() + "枚目 (dataid:" + arg.dataInfoDataid + ")";
            backgroundWorker1.RunWorkerAsync(arg);
        }

        /// <summary>
        /// 読み込みのバックグラウンド処理処理 に 渡す引数を格納するクラス
        /// </summary>
        class backgroundArg
        {
            /// <summary> ルートのディレクトリ (datainfo.txtのあるディレクトリ) </summary>
            public string rootDir = "";

            /// <summary> datainfo.txtの date: </summary>
            public string dataInfoDataid = "";

            /// <summary> datainfo.txtの discno: </summary>
            public uint dataInfoDiscno = 0;

            /// <summary> datainfo.txtの disccount: </summary>
            public uint dataInfoDisccount = 0;

            public object logno = null;
        }

        /// <summary>
        /// 読み込みのバックグラウンド処理処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> backgroundArg でキャストする</param>
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            backgroundArg arg = (backgroundArg)e.Argument;      // 入力 backgroundArg でキャストする

            e.Cancel = false;                                   // キャンセル = false
            
            // CSVファイル用のバッファへの読み込み
            // (行数カウントのために、全行を一気に読む)
            int cvsBuffCnt = 0;                                 // CVSのバッファの行数 （プログレスバーの為にカウントさせる）
#if !DEBUG
            try
#endif
            {
                string[] csvPath = Directory.GetFiles(arg.rootDir, "*.csv");
                if (csvPath.Length > 0)
                {
                    string[] csvBuff = File.ReadAllLines(csvPath[0], Encoding.GetEncoding("Shift_JIS"));   // 全読み込み
                    cvsBuffCnt = csvBuff.Length;


#if SQLIGHT
                    using(var tran = C_GlobalData.sqlBeginTransaction())
#endif
                    {
                        for (int i = 0; i < cvsBuffCnt; i++)
                        {
                            if (((BackgroundWorker)sender).CancellationPending)                     // キャンセル中断
                            {
                                if (arg.logno != null)
                                {
                                    C_GlobalData.sqlWrite("UPDATE t_ReadLog SET col_info = '{0}' WHERE col_logno = {1}", "CSV読込中断", arg.logno);
                                }

                                e.Cancel = true;
                                return;
                            }
                            csvLine_2_DB(csvBuff[i]);

                            // プログラスバーへの反映 （最大で20%）
                            ((BackgroundWorker)sender).ReportProgress(i * 20 / cvsBuffCnt);
                        }
#if SQLIGHT
                        tran.Commit();
#endif
                    }
                }
            }
#if !DEBUG
            catch (Exception)
            {
                if (arg.logno != null)
                {
                    C_GlobalData.sqlWrite("UPDATE t_ReadLog SET col_info = '{0}' WHERE col_logno = {1}", "CSV読込エラー", arg.logno);
                }
            }
#endif

            // t_fileseek テーブルに書き込むデータ一覧　（キー：filename, データ：foldername）
            var t_fileseek = new Dictionary<string, string>();
#if !DEBUG
            try
#endif
            {
                // ファイル一覧のバッファ (コピーするファイル数カウントのために、全行を一気に読む)
                List<string> fullPathList = new List<string>();

                // ルート (datainfo.txt と同じディレクトリの直下) のファイル一覧を追加する
                foreach (string file in Directory.GetFiles(arg.rootDir))
                {
                    fullPathList.Add(file);    
                }

                // サブディレクトリのファイル一覧を追加する
                foreach (string sub in Directory.GetDirectories(arg.rootDir))   // サブ   = ルート上のサブディレクトリ (img_●●)
                {
                    foreach (string pathName in Directory.GetFiles(sub))        // ファイル = サブディレクトリ上の全ファイル
                    {
                        fullPathList.Add(pathName);
                    }
                }


                // ファイルのコピー
                int fileCount = fullPathList.Count;
                for (int i = 0; i < fileCount; i++)
                {
#if !DEBUG
                    try
#endif
                    {
                        string file = Path.GetFileName(fullPathList[i]);            // ファイル名
                        string fromDir = Path.GetDirectoryName(fullPathList[i]);    // 入力ディレクトリ (例：D:\img_01)
                        string sub = fromDir.Replace(arg.rootDir, "");              // サブ             (例:img_01)

                        // 出力先ディレクトリ
                        string outDir = arg.dataInfoDataid + @"\" +                                 // dataid( 例：20120210\ )
                                        "disc" + arg.dataInfoDiscno.ToString("00");                 // discno( 例：disc01   )
                        if (sub.Length > 0) outDir += @"\" + sub;                                   // sub   ( 例:\img)

                        // 画像フォルダ名をDBに書き込む
                        t_fileseek[file] = outDir;
                        
                        // ファイルをコピーする
                        Directory.CreateDirectory(C_PostgreSettings.PictureRootPath + "\\" + outDir);
                        File.Copy(fullPathList[i], C_PostgreSettings.PictureRootPath + "\\" + outDir + "\\" + file, true);


                        // キャンセル中断
                        if (((BackgroundWorker)sender).CancellationPending)
                        {
                            if (arg.logno != null)
                            {
                                C_GlobalData.sqlWrite("UPDATE t_ReadLog SET col_info = '{0}' WHERE col_logno = {1}", "画像取り込み中断", arg.logno);
                            }
                            e.Cancel = true;
                            Write_2_fileseek(t_fileseek);       // 途中までの画像コピーを、DBに反映させる
                            return;
                        }

                        int progress;
                        if (cvsBuffCnt > 0)
                        {
                            progress = ((i + 1) * 80 / fileCount) + 20;
                        }
                        else
                        {
                            progress = ((i + 1) * 100 / fileCount);
                        }

                        // プログラスバーへの反映 （最大で20%）
                        ((BackgroundWorker)sender).ReportProgress(progress);


                    }
#if !DEBUG
                    catch (Exception)
                    {
                    }
#endif
                }
            }
#if !DEBUG
            catch (Exception)
            {
            }
#endif

            Write_2_fileseek(t_fileseek);   // 画像コピーを、DBに反映させる


            // バックグラウンド処理完了 ⇒ キャンセルではない！
            e.Cancel = false;

            // 今回読み込んだディスクの datainfo.txt の情報を書き込む
            C_GlobalData.writeDataInfoForXML(arg.dataInfoDataid, arg.dataInfoDiscno, arg.dataInfoDisccount);


            if (arg.logno != null)
            {
                C_GlobalData.sqlWrite("UPDATE t_ReadLog SET col_info = '{0}' WHERE col_logno = {1}", "画像取り込み完了", arg.logno);
            }
        }


        void Write_2_fileseek(Dictionary<string, string> t_fileseek)
        {
#if SQLIGHT
            using (var tran = C_GlobalData.sqlBeginTransaction())
#endif
            {
                foreach (var file in t_fileseek.Keys)
                {
                    var outDir = t_fileseek[file];
                    string[] dbCol = { "filename", "foldername" };
                    string[] dbDat = { "'" + file + "'", "'" + outDir + "'" };
                    C_GlobalData.sqlInsert("t_fileseek", dbCol, dbDat);
                }
#if SQLIGHT
                tran.Commit();
#endif
            }
        }

        /// <summary>
        /// OKボタンバックグラウンドの終了処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar.Value = 0;

            if (e.Cancelled)            // キャンセルボタン
            {
                this.Close();           // ダイアログを閉じる
                return;
            }
            
            // OKボタンを有効に戻す
            buttonOK.Enabled = true;

            // 読み込み完了のメッセージを表示
            string date; uint discno;  uint disccount;
            C_GlobalData.getDataInfoFromXML(out date, out discno, out disccount);

            if (discno < disccount)
            {
                MessageBox.Show(discno.ToString() + "枚目まで読み込みました。 (dataid:" + date + ")");
                label_Message_1.Text = (discno + 1).ToString() + "枚目のディスクをセットしてください";
                label_Message_2.Text = "前回：" + discno.ToString() + "枚目 (date:" + date + ")";
            }
            else
            {
                MessageBox.Show(discno.ToString() + "枚すべての読み込みが完了しました。 (dataid:" + date + ")");
                this.Close();
            }
        }

        /// <summary>
        /// CSV1行分のデータを、DBに出力する
        /// </summary>
        /// <param name="line">CSV1行のデータ</param>
        void csvLine_2_DB(string line)
        {
            // 出力先の列の名前一覧
            string[] dbOutColName = { "id",                             // 電算管理番号
                                      "filename",                       // 画像ファイル名
                                      "zokushi",                        // 続紙枚数 （画像ファイル件数）
                                    };
            try
            {
                string[] csvData = line.Split(',');                     // DVDから読み込んだデータを、カンマで分割
                string[] dbOutColData = { "'" + csvData[0] + "'",       // 電算管理番号
                                          "'" + csvData[1] + "'",       // 画像ファイル名
                                          csvData[2],                   // 画像ファイル件数
                                        };

                // SQLコマンド（insert文 or update分）で、列を追加・更新
                C_GlobalData.sqlInsert("t_pictures", dbOutColName, dbOutColData);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// キャンセルボタン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// ProgressBarの更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private Queue<string> m_logQue = new Queue<string>();

        private void backgroundWorker_ForLog_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Threading.Monitor.Enter(m_logQue);
            System.Threading.Monitor.Exit(m_logQue);
        }



    }
}
